#!/usr/bin/env ruby

require "puppeteer"

URL = 'http://oregonliquorsearch.com/home.jsp'
WINDOW_SIZE_HEIGHT = 1280
WINDOW_SIZE_WIDTH = 1280

product = '8722B'
postalcode = '97214'

if ARGV.length > 0
  product = ARGV[0]
end

Puppeteer.launch(headless: true, args: ["--window-size=%s,%s" % [WINDOW_SIZE_WIDTH, WINDOW_SIZE_HEIGHT]]) do |browser|
  page = browser.pages.first || browser.new_page
  page.viewport = Puppeteer::Viewport.new(
    width: WINDOW_SIZE_WIDTH,
    height: WINDOW_SIZE_HEIGHT
  )
  page.goto(URL, wait_until: 'domcontentloaded')

  yes21 = page.query_selector('input[name=btnSubmit]')

  await_all(
    page.async_wait_for_navigation,
    yes21.async_click
  )

  page.wait_for_selector('input[name=btnSearch]')

  page.query_selector('input[name=productSearchParam]').type_text(product)
  page.query_selector('input[name=locationSearchParam]').type_text(postalcode)

  await_all(
    page.async_wait_for_navigation,
    page.query_selector('input[name=btnSearch]').async_click
  )

  page.wait_for_selector('div[id="page-title"]')

  page.screenshot(path: "snapshots/%s.png" % Time.now.strftime('%Y-%m-%dT%H-%M-%S.%L%z'))
end
